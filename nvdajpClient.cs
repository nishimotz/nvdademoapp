﻿using System;
using System.Runtime.InteropServices;

class nvdajpClient
{
    public static bool Is64bit()
    {
        if (IntPtr.Size == 8)
        {
            return true;
        }
        else if (IntPtr.Size == 4)
        {
            return false;
        }
        throw new System.Exception("unsupported system");
    }

    /* [comm_status][fault_status] error_status_t __stdcall nvdaController_testIfRunning(void); */

    [DllImport("nvdaControllerClient32.dll", EntryPoint = "nvdaController_testIfRunning")]
    extern static uint nvdaController32_testIfRunning();

    [DllImport("nvdaControllerClient64.dll", EntryPoint = "nvdaController_testIfRunning")]
    extern static uint nvdaController64_testIfRunning();

    public static bool IsRunning()
    {
        if (Is64bit())
        {
            return nvdaController64_testIfRunning() == 0;
        }
        return nvdaController32_testIfRunning() == 0;
    }

    /* [comm_status][fault_status] error_status_t __stdcall nvdaController_speakText( [string][in] const wchar_t *text); */

    [DllImport("nvdaControllerClient32.dll", EntryPoint = "nvdaController_speakText", CharSet = CharSet.Unicode)]
    static extern uint nvdaController32_speakText(String text);

    [DllImport("nvdaControllerClient64.dll", EntryPoint = "nvdaController_speakText", CharSet = CharSet.Unicode)]
    static extern uint nvdaController64_speakText(String text);

    public static bool SpeakText(string text)
    {
        if (Is64bit())
        {
            return nvdaController64_speakText(text) == 0;
        }
        return nvdaController32_speakText(text) == 0;
    }

    /* [comm_status][fault_status] error_status_t __stdcall nvdaController_cancelSpeech(void); */

    [DllImport("nvdaControllerClient32.dll", EntryPoint = "nvdaController_cancelSpeech")]
    static extern uint nvdaController32_cancelSpeech();

    [DllImport("nvdaControllerClient64.dll", EntryPoint = "nvdaController_cancelSpeech")]
    static extern uint nvdaController64_cancelSpeech();

    public static bool CancelSpeech()
    {
        if (Is64bit())
        {
            return nvdaController64_cancelSpeech() == 0;
        }
        return nvdaController32_cancelSpeech() == 0;
    }

    /* [comm_status][fault_status] error_status_t __stdcall nvdaController_brailleMessage( [string][in] const wchar_t *message); */

    [DllImport("nvdaControllerClient32.dll", EntryPoint = "nvdaController_brailleMessage", CharSet = CharSet.Unicode)]
    static extern uint nvdaController32_brailleMessage(String text);

    [DllImport("nvdaControllerClient64.dll", EntryPoint = "nvdaController_brailleMessage", CharSet = CharSet.Unicode)]
    static extern uint nvdaController64_brailleMessage(String text);

    public static bool BrailleMessage(string text)
    {
        if (Is64bit())
        {
            return nvdaController64_brailleMessage(text) == 0;
        }
        return nvdaController32_brailleMessage(text) == 0;
    }

    /* [comm_status][fault_status] error_status_t __stdcall nvdaController_speakSpelling( [string][in] const wchar_t *text); */

    [DllImport("nvdaControllerClient32.dll", EntryPoint = "nvdaController_speakSpelling", CharSet = CharSet.Unicode)]
    static extern uint nvdaController32_speakSpelling(String text);

    [DllImport("nvdaControllerClient64.dll", EntryPoint = "nvdaController_speakSpelling", CharSet = CharSet.Unicode)]
    static extern uint nvdaController64_speakSpelling(String text);

    public static bool SpeakSpelling(string text)
    {
        if (Is64bit())
        {
            return nvdaController64_speakSpelling(text) == 0;
        }
        return nvdaController32_speakSpelling(text) == 0;
    }

    /* [comm_status][fault_status] error_status_t __stdcall nvdaController_isSpeaking(void); */

    [DllImport("nvdaControllerClient32.dll", EntryPoint = "nvdaController_isSpeaking")]
    extern static uint nvdaController32_isSpeaking();

    [DllImport("nvdaControllerClient64.dll", EntryPoint = "nvdaController_isSpeaking")]
    extern static uint nvdaController64_isSpeaking();

    public static bool IsSpeaking()
    {
        if (Is64bit())
        {
            return nvdaController64_isSpeaking() != 0;
        }
        return nvdaController32_isSpeaking() != 0;
    }

    /* [comm_status][fault_status] error_status_t __stdcall nvdaController_getPitch(void); */

    [DllImport("nvdaControllerClient32.dll", EntryPoint = "nvdaController_getPitch")]
    extern static uint nvdaController32_getPitch();

    [DllImport("nvdaControllerClient64.dll", EntryPoint = "nvdaController_getPitch")]
    extern static uint nvdaController64_getPitch();

    public static int GetPitch()
    {
        if (Is64bit())
        {
            return (int)nvdaController64_getPitch();
        }
        return (int)nvdaController32_getPitch();
    }

    /* [comm_status][fault_status] error_status_t __stdcall nvdaController_setPitch(const int nPitch); */

    [DllImport("nvdaControllerClient32.dll", EntryPoint = "nvdaController_setPitch")]
    static extern uint nvdaController32_setPitch(int pitch);

    [DllImport("nvdaControllerClient64.dll", EntryPoint = "nvdaController_setPitch")]
    static extern uint nvdaController64_setPitch(int pitch);

    public static bool SetPitch(int pitch)
    {
        if (Is64bit())
        {
            return nvdaController64_setPitch(pitch) == 0;
        }
        return nvdaController32_setPitch(pitch) == 0;
    }

    /* [comm_status][fault_status] error_status_t __stdcall nvdaController_getRate(void); */

    [DllImport("nvdaControllerClient32.dll", EntryPoint = "nvdaController_getRate")]
    extern static uint nvdaController32_getRate();

    [DllImport("nvdaControllerClient64.dll", EntryPoint = "nvdaController_getRate")]
    extern static uint nvdaController64_getRate();

    public static int GetRate()
    {
        if (Is64bit())
        {
            return (int)nvdaController64_getRate();
        }
        return (int)nvdaController32_getRate();
    }

    /* [comm_status][fault_status] error_status_t __stdcall nvdaController_setRate(const int nRate); */

    [DllImport("nvdaControllerClient32.dll", EntryPoint = "nvdaController_setRate")]
    static extern uint nvdaController32_setRate(int rate);

    [DllImport("nvdaControllerClient64.dll", EntryPoint = "nvdaController_setRate")]
    static extern uint nvdaController64_setRate(int rate);

    public static bool SetRate(int rate)
    {
        if (Is64bit())
        {
            return nvdaController64_setRate(rate) == 0;
        }
        return nvdaController32_setRate(rate) == 0;
    }

    /* [comm_status][fault_status] error_status_t __stdcall nvdaController_setAppSleapMode(const int mode); */

    [DllImport("nvdaControllerClient32.dll", EntryPoint = "nvdaController_setAppSleepMode")]
    static extern uint nvdaController32_setAppSleepMode(int mode);

    [DllImport("nvdaControllerClient64.dll", EntryPoint = "nvdaController_setAppSleepMode")]
    static extern uint nvdaController64_setAppSleepMode(int mode);

    public static bool SetAppSleepMode(bool mode)
    {
        if (Is64bit())
        {
            return nvdaController64_setAppSleepMode(mode ? 1 : 0) == 0;
        }
        return nvdaController32_setAppSleepMode(mode ? 1 : 0) == 0;
    }

}

