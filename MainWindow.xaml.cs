﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using nc = nvdajpClient;

namespace NvdaDemoApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public DispatcherTimer dispatcherTimer { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            dispatcherTimer = new DispatcherTimer(DispatcherPriority.Normal);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 500);
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
        }

        private void speakButton_Click(object sender, RoutedEventArgs e)
        {
            if (!nc.IsRunning())
            {
                msgText.Text = "nvda is not running";
                return;
            }
            nc.SpeakText("NVDAが動いています。");
        }

        private void cancelSpeechButton_Click(object sender, RoutedEventArgs e)
        {
            if (!nc.IsRunning())
            {
                msgText.Text = "nvda is not running";
                return;
            }
            nc.SpeakText("NVDAが動いています。NVDAが動いています。NVDAが動いています。");
            System.Threading.Thread.Sleep(2000);
            nc.CancelSpeech();
        }

        private void speakSpellingButton_Click(object sender, RoutedEventArgs e)
        {
            if (!nc.IsRunning())
            {
                msgText.Text = "nvda is not running";
                return;
            }
            nc.SpeakSpelling("カタカナ ひらがな NVDAが動いています。");
        }

        private void timerStartButton_Click(object sender, RoutedEventArgs e)
        {
            dispatcherTimer.Start();
            msgText.Text = "timer started";
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            string s;
            if (nc.IsRunning())
            {
                if (nc.IsSpeaking())
                {
                    s = "nvda is running and speaking.";
                }
                else
                {
                    s = "nvda is running.";
                }
            }
            else
            {
                s = "nvda is not running.";
            }
            if (nc.Is64bit())
            {
                s += " I am 64bit.";
            }
            else
            {
                s += " I am 32bit.";
            }
            msgText.Text = s;
        }

        private void timerStopButton_Click(object sender, RoutedEventArgs e)
        {
            dispatcherTimer.Stop();
            msgText.Text = "timer stopped";
        }

        private async void pitchDemoButton_Click(object sender, RoutedEventArgs e)
        {
            if (!nc.IsRunning())
            {
                msgText.Text = "nvda is not running";
                return;
            }
            var pitch = nc.GetPitch();
            msgText.Text = "Pitch " + pitch.ToString();
            nc.SpeakText("NVDAが動いています。NVDAが動いています。NVDAが動いています。");
            await Task.Delay(3000);
            nc.CancelSpeech();
            nc.SetPitch(pitch + 20);
            msgText.Text = "Pitch " + (pitch + 20).ToString();
            nc.SpeakText("NVDAが動いています。NVDAが動いています。NVDAが動いています。");
            await Task.Delay(3000);
            nc.CancelSpeech();
            nc.SetPitch(pitch);
            msgText.Text = "Pitch " + (pitch).ToString();
            nc.SpeakText("NVDAが動いています。NVDAが動いています。NVDAが動いています。");
            await Task.Delay(3000);
            nc.CancelSpeech();
        }

        private async void rateDemoButton_Click(object sender, RoutedEventArgs e)
        {
            if (!nc.IsRunning())
            {
                msgText.Text = "nvda is not running";
                return;
            }
            var rate = nc.GetRate();
            msgText.Text = "Rate " + rate.ToString();
            nc.SpeakText("NVDAが動いています。NVDAが動いています。NVDAが動いています。");
            await Task.Delay(3000);
            nc.CancelSpeech();
            nc.SetRate(100);
            msgText.Text = "Rate 100";
            nc.SpeakText("NVDAが動いています。NVDAが動いています。NVDAが動いています。");
            await Task.Delay(3000);
            nc.CancelSpeech();
            nc.SetRate(rate);
            msgText.Text = "Rate " + (rate).ToString();
            nc.SpeakText("NVDAが動いています。NVDAが動いています。NVDAが動いています。");
            await Task.Delay(3000);
            nc.CancelSpeech();
        }

        private void setAppSleepButton_Click(object sender, RoutedEventArgs e)
        {
            if (!nc.IsRunning())
            {
                msgText.Text = "nvda is not running";
                return;
            }
            nc.SetAppSleepMode(true);
        }

        private void unsetAppSleepButton_Click(object sender, RoutedEventArgs e)
        {
            if (!nc.IsRunning())
            {
                msgText.Text = "nvda is not running";
                return;
            }
            nc.SetAppSleepMode(false);
        }

        private void brailleMessageButton_Click(object sender, RoutedEventArgs e)
        {
            if (!nc.IsRunning())
            {
                msgText.Text = "nvda is not running";
                return;
            }
            nc.BrailleMessage("NVDAが動いています。");
        }
    }
}
